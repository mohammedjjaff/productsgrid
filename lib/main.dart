import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage()
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<product> products = [
    product(name: 'منتج اول', img: 'img/img.png', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 30,000'),
    product(name: 'منتج ثاني', img: 'img/img2.png', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 50,000'),
    product(name: 'منتج ثالث', img: 'img/img3.png', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 60,000'),
    product(name: 'منتج رابع', img: 'img/img4.png', description: 'وصف للمنتج وصف للمنتج وصف للمنتج', price: 'IQD 70,000'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('الرئيسية',textAlign: TextAlign.center,
        style: GoogleFonts.almarai(fontSize: 16,color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        leading: const Icon(Icons.person_outline_sharp,color: Colors.black,),
        actions: [
          IconButton(onPressed: (){}, icon: const Icon(Icons.drag_indicator,color: Colors.black,))
        ],
      ),
      body: Stack(
        children: [
          Positioned.fill(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: ListView(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 110,
                    child: ListView.builder(
                      itemCount: products.length,
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      reverse: true,
                      itemBuilder: (context,index){
                        return Container(
                          width: 70,
                          height: 70,
                          margin: const EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.asset(products[index].img,fit: BoxFit.fill,width: 70,height: 70,),
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                width: 70,
                                child: Text(products[index].name,textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,
                                style: GoogleFonts.almarai(color: Colors.black),),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  CarouselSlider(
                    options: CarouselOptions(
                        height: 200.0,
                        enlargeCenterPage: true,
                        autoPlay: true,
                        pageSnapping:false,
                        aspectRatio: 16/19,
                        enableInfiniteScroll: false,
                        viewportFraction: 1
                    ),
                    items: products.map((e){
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              bottom: 20,
                              child: InkWell(
                                onTap: ()async{},
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.red
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.asset(e.img,fit: BoxFit.fitWidth,),
                                    )
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: SizedBox(
                                height: 60,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: 20.0,
                                      sigmaY: 20.0,
                                    ),
                                    child: AnimatedContainer(
                                      duration: const Duration(milliseconds: 250),
                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.95).withOpacity(0.2),
                                          borderRadius: BorderRadius.circular(20),
                                          border: Border.all(color: Colors.grey.withOpacity(0.2))
                                      ),
                                      child: Center(
                                        child: Text(e.name,textAlign: TextAlign.right,
                                          style: GoogleFonts.almarai(fontSize: 15,color: Colors.black,fontWeight: FontWeight.bold),),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  GridView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(horizontal: 1,vertical: 8),
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1,
                        mainAxisExtent: 260,
                        crossAxisSpacing: 20
                    ),
                    children: products.map((product element){
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              top: 0,
                              bottom: 40,
                              left: 10,
                              right: 10,
                              child: Container(
                                height: 110,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.red,
                                    image: DecorationImage(
                                        image: AssetImage(element.img),
                                        fit: BoxFit.cover
                                    )
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: SizedBox(
                                height: 60,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: 20.0,
                                      sigmaY: 20.0,
                                    ),
                                    child: AnimatedContainer(
                                      duration: const Duration(milliseconds: 250),
                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.95).withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(color: Colors.grey.withOpacity(0.2))
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Icon(Icons.favorite,color: Colors.red,),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Text(element.name,textAlign: TextAlign.right,
                                              style: GoogleFonts.almarai(fontSize: 15,color: Colors.black,fontWeight: FontWeight.bold),),
                                              const SizedBox(height: 5,),
                                              Text(element.price,textAlign: TextAlign.right,
                                                style: GoogleFonts.almarai(fontSize: 15,color: Colors.green,fontWeight: FontWeight.bold),),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 350),
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              height: 70,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 24,
                        spreadRadius: 16,
                        color: Colors.black.withOpacity(0.2)
                    )
                  ]
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(0),
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 20.0,
                    sigmaY: 20.0,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 75,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 3,horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                              onTap: (){

                              },
                              child: AnimatedContainer(
                                duration: const Duration(milliseconds: 250),
                                width: 80,
                                height: 35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.transparent
                                ),
                                child: const Icon(Icons.category,size: 27,),
                              )
                          ),
                          InkWell(
                            onTap: (){

                            },
                            child: AnimatedContainer(
                                duration: const Duration(milliseconds: 250),
                                width: 80,
                                height: 35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent
                                ),
                                child: const Icon(Icons.home,color: Colors.white,size: 27,)),
                          ),
                          InkWell(
                            onTap: (){

                            },
                            child: AnimatedContainer(
                                duration: const Duration(milliseconds: 250),
                                width: 80,
                                height: 35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.transparent
                                ),
                                child: const Icon(Icons.person,size: 27,)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}




class product{
  String name;
  String price;
  String img;
  String description;
  product({required this.name,required this.img,required this.description,required this.price});
}